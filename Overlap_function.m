function Overlap_function(cases)

switch cases
    case 'areahistogram'
        areahistogram();
    case 'circularity_histogram'
        circularity_histogram();
    case 'test_expansion'
        test_expansion();
    case 'run_counter'
        run_counter();
    case 'count_steps'
        count_steps();
    case 'intensity'
        intensity();
end

function run_counter()
Counter=findobj('Tag','Counter');
edit_circularity=findobj('tag','edit_circularity');
maxcircularity=str2double(get(edit_circularity,'String'));
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));
edit_expansion=findobj(Counter,'Tag','edit_expansion');
expansion=str2double(get(edit_expansion,'string'));

Data=get(Counter,'Userdata');

%load the blackandwhite image
[imgBW,img1]=Blackandwhite(Data);
imgBW = bwareaopen(imgBW, minarea); %ignore smaller then minarea pixels

%calculate the image size
imsize=[size(imgBW,1),size(imgBW,2)];

%transform the locations so that they overlap with the BWimage
[X,Y,pos]=load_gaussian(Data,imsize);

%if there are negative fits (due to blinking) store the values in Xpos and
%Ypos
if ~isempty(pos)
    Xpos=pos(:,2);
    Ypos=pos(:,3); 
end

%If the image is zoomed there is a data field x_box (and y_box)
%indicating the zoomed field, cut both the high res image and the black
%and white image
if isfield(Data,'x_box')
    magy=round(imsize(1)/Data.yaxis);
    magx=round(imsize(2)/Data.xaxis);

    y_box=Data.y_box;
    x_box=Data.x_box;
    X=X-min(x_box)*magx;
    Y=Y-min(y_box)*magy;
    if ~isempty(pos)
        Xpos=Xpos-min(x_box)*magx;
        Ypos=Ypos-min(y_box)*magy; 
    end
    img1=img1(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
    imgBW=imgBW(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
end

%calculate the boundaries of BW image
[B,L] = bwboundaries(~imgBW);

%calculate the area of the regions
stats = regionprops(L,'Area','Centroid');

%initialize some vectors
numbers=[];
allin=ones([length(X),1]);
if size(B)>0
    numbers(length(B))=0;
else
    numbers=[];
end
plasmids=0;
labelledplasmids=0;
circularity=ones(length(B),1).*-1; %initialize to -1
circularity(1)=-1;
area=zeros(length(B),1);
perimeter=zeros(length(B),1);


%plot the black and white image
figure(1);imshow(imgBW)

%plot the high resolution image
mx=max(max(img1));
mn=min(min(img1));
figure(2);imshow(img1,[mn mx])
%keep hold on to include the boundaries and overlay the 
hold on

for k = 2:length(B) %first boundary is complete image, ignore it
    boundary = B{k};
    area(k) = stats(k).Area;
    
    %calculate the perimeter
    delta_sq = diff(boundary).^2; 
    perimeter(k) = sum(sqrt(sum(delta_sq,2)));
    
    %Check if the perimeter is bigger than 0 (otherwise there is no
    %area value) and the area is bigger than the minimal area
    if perimeter(k)>0 && area(k)>minarea
        %Calculate the circularity
        circularity(k)=perimeter(k).^2/(4*pi*area(k));
        %Check if circularity is smaller than set circularity (note can
        %include a 0 check to disable this in case of non-circular
        %molecules)
        if circularity(k)<maxcircularity
            xboundary=boundary(:,2);yboundary=boundary(:,1);
            %Increase the boundaries of the spots (This currently only works for circular spots,
            %might need to change this)
            [Xboundary,Yboundary]=increaseboundaries(xboundary,yboundary,expansion); 
            
            %plot the boundaries
            plot(Xboundary, Yboundary, 'r', 'LineWidth', 0.2)
            
            %check which fits are inside the set boundaries
            in=inpolygon(X,Y,Xboundary,Yboundary);
            
            %plot the locations inside the boundaries
            plot(X(in>0),Y(in>0),'mx','markersize',6);
            
            %if there are negative fits
            if ~isempty(pos)
                %are there negative fits inside the boundaries
                inpos=inpolygon(Xpos,Ypos,Xboundary,Yboundary);
                
                %total labels is sum of fits inside boundary - sum of
                %negative fits inside the fits
                labels=sum(in)-sum(inpos);
                
                %plot the negative fit locations inside the boundaries
                plot(Xpos(inpos>0),Ypos(inpos>0),'gx','markersize',6);
            else
                
                %total labels in this plasmid is the sum of the locations
                %localized inside the boundaries
                labels=sum(in);
            end
            
            %subtract the vector of plasmids that are in the boundaries
            %(the index of locations that are inside the boundaries is set
            %to 1) from the vector with size length locations, all set to 1
            allin=allin-in;
            
            %spot k has "labels" labels
            numbers(k)=labels;
            %if this value is higher than 0, than the total spots that has
            %a location associated with it increases with 1
            if labels>0; labelledplasmids=labelledplasmids+1;end
            
            %total spots is increased by 1
            plasmids=plasmids+1;
        end
    end
end
%indicate the total |Boundaries found
text(30,30,strcat('\color{yellow}Objects Found:',num2str(length(B))))

%plot the locations that are not found (allin=1) inside a plasmid 
plot(X(allin>0),Y(allin>0),'bx','markersize',6);
hold off

%initialize vector containing 
labels_sum(1:10)=0;
for i=1:max(numbers)+1
    labels_sum(i)=sum(numbers(:)==i-1);
end

%for saving purposes
C=labels_sum';

%display results
disp('')
disp(['Total plasmids: ' num2str(plasmids)])
disp(['no labels: ' num2str(plasmids-labelledplasmids) ' (' num2str(100*(1-labelledplasmids/plasmids)) '%)'])
disp(['1 label: ' num2str(labels_sum(2)) ' (' num2str(100*labels_sum(2)/plasmids) '%)'])
totallabels=labels_sum(2);
for i = 3:size(labels_sum,2)
    if labels_sum(i)>0
        disp([num2str(i-1) ' labels: ' num2str(labels_sum(i)) ' (' num2str(100*labels_sum(i)/plasmids) '%)'])
    end
    totallabels=totallabels+(i-1)*labels_sum(i);
end
freedye=size(allin,1)-totallabels;
disp(['free dye: ' num2str(freedye)]);

C=[freedye;plasmids; plasmids-labelledplasmids;  C(2:end)];

%save the file with the total number of locations per spot (first entry is
%number of free dyes, the 2nd entry is the number of plasmids, 3rd entry is
%number of plasmids without labels, then the plasmids with labels in order
save('temp2.mat', 'C');

function count_steps()
Counter=findobj('Tag','Counter');
edit_circularity=findobj('tag','edit_circularity');
maxcircularity=str2double(get(edit_circularity,'String'));
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));
edit_expansion=findobj(Counter,'Tag','edit_expansion');
expansion=str2double(get(edit_expansion,'string'));
edit_horizontal=findobj(Counter,'Tag','edit_horizontal');
edit_vertical=findobj(Counter,'Tag','edit_vertical');
horizontal=str2double(get(edit_horizontal,'string'));
vertical=str2double(get(edit_vertical,'string'));
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));

Data=get(Counter,'Userdata');

%Load black and white image of high resolution image
[imgBW,img1]=Blackandwhite(Data);

%calculate the image size
imsize=[size(imgBW,1),size(imgBW,2)];

%calculate the magnification
magy=round(imsize(1)/Data.yaxis);
magx=round(imsize(2)/Data.xaxis);

%Load the bleaching image
fn3=Data.fn3;
fp3=Data.fp3;
bleaching_img=imread([fp3 fn3],1);
[~ ,fr]=imread([fp3 fn3],1);

%rotate the bleaching image
bleaching_img=flipdim(bleaching_img,1)./attenuate;
maxframe=5;
for i=2:maxframe
    img=imread([fp3 fn3],i);
    img=flipdim(img,1);
    bleaching_img=bleaching_img+img;
end

bleaching_img=bleaching_img./maxframe;

%transform the locations so that they overlap with the BWimage
[X,Y,~]=load_gaussian(Data,imsize);

%If the image is zoomed there is a data field x_box (and y_box)
%indicating the zoomed field, cut both the high res image and the black
%and white image
if isfield(Data,'x_box')
        y_box=Data.y_box;
        x_box=Data.x_box;
        X=X-min(x_box)*magx;
        Y=Y-min(y_box)*magy;
        
        img1=img1(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
        imgBW2=imgBW2(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);

end

imgBW2 = ~bwareaopen(imgBW, minarea); %ignore smaller then minarea pixels

%calculate the boundaries of BW image
[B,L] = bwboundaries(imgBW2);

%calculate the area of the regions
stats = regionprops(L,'Area','Centroid');

%plot black and white image
figure(3);imshow(imgBW2)

%include waitbar
h = waitbar(0,'fitting frame: 0');
x=1;

%initialize some vectors
plasmids=0;
labelledplasmids=0;
labelledplasmids2=0;
if size(B)>0
    numbers2(length(B))=0;
    numbers(length(B))=0;
else
    numbers=[];numbers2=[];
end
circularity=ones(length(B),1).*-1; %initialize to -1
circularity(1)=-1;
area=zeros(length(B),1);
perimeter=zeros(length(B),1);


%plot the high resolution image 
mx=0.6*max(max(img1));
mn=min(min(img1));
figure(2);imshow(img1,[mn mx])
hold on
text(0,0,strcat('\color{yellow}BALM image'))
hold off

%plot the bleaching image
mx=max(max(bleaching_img));
mn=min(min(bleaching_img));
figure(4);imagesc(bleaching_img,[mn mx]);colormap('gray')
hold on
text(0,0,strcat('\color{yellow}Bleaching image'))
hold off

for k = 2:length(B) %first boundary is complete image
    boundary = B{k};
    area(k) = stats(k).Area;
    
    %update the waitbar
    waitbar(k/length(B),h,['fitting frame:' num2str(k) ' of ' num2str(length(B))]);
    
    %calculate the perimeter
    delta_sq = diff(boundary).^2; 
    perimeter(k) = sum(sqrt(sum(delta_sq,2)));
    
    %Check if the perimeter is bigger than 0 (otherwise there is no
    %area value) and the area is bigger than the minimal area

    if perimeter(k)>0
        circularity(k)=perimeter(k).^2/(4*pi*area(k));
        if circularity(k)<maxcircularity
            xboundary=boundary(:,2);yboundary=boundary(:,1);
            %Increase the boundaries of the spots (This currently only works for circular spots,
            %might need to change this)
            [Xboundary,Yboundary]=increaseboundaries(xboundary,yboundary,expansion); 
            
            %create the boundaries associated with the bleaching image
            Xboundary2=(Xboundary/magx)+2; Yboundary2=(Yboundary/magy)+2;
            
            %create a mask for the bleaching image
            mask = roipoly(bleaching_img,Xboundary2,Yboundary2);
            %Intensity(k)=mean(bleaching_img(mask));
            %mask = poly2mask(Xboundary,Yboundary,length1,length2);
            
            %plot the locations on the bleaching figure and the high
            %resolution image
            figure(4)
            hold on 
            plot(Xboundary2, Yboundary2, 'r', 'LineWidth', 0.2)
            hold off
            
            figure(2)
            hold on
            plot(Xboundary, Yboundary, 'r', 'LineWidth', 0.2)
            
            %check how many spots are in the boundaries
            in=inpolygon(X,Y,Xboundary,Yboundary);
            labels=sum(in);
            plot(X(in>0),Y(in>0),'mx','markersize',6);
            hold off
            numbers2(k)=labels;
            
            
            
            disp(['labels on ' num2str(k) ':' num2str(labels)]);
            if labels>0; labelledplasmids2=labelledplasmids2+1;end
            begin=4;
            
            %calculate the intensity trace
            for i=begin:fr
                im=imread([fp3 fn3],i);
                Intensity(i-begin+1)=mean(im(mask));
                
            end
            
            %%Filtering
            windowsize=3;
            X_axis=begin:fr;
            int_filter=filter(ones(1,windowsize)/windowsize,1,Intensity);
            figure(6);clf('reset')
            
            %plot the intensity after filtering
            plot(X_axis,int_filter,'b-')
            hold on
            %plot the intensity before filtering
            plot(X_axis,Intensity,'--g')
            ylim([min(Intensity(windowsize:fr-begin)),max(Intensity(windowsize:fr-begin))])
            histo_int(x)=max(int_filter);
            x=x+1;
            
            %%Edge detection
            [WT,S]=mal_fwt(5, int_filter);
            ea=prod(WT(:,1:5)')./(max(Intensity)*10000);
            
            %addaxis(X_axis,ea,[0,1000],'--k')
            ea(ea.^2<1000^2)=0;
            eadiff=diff(ea);
            eadiff(eadiff<0)=-1;
            eadiff(eadiff>0)=1;
            eadiff2=diff(eadiff);
            pospeak=find(eadiff2==-2);
            negpeak=find(eadiff2==2);
            
            %create a step graph where each positive peak is a step upward
            %and a negative peak is a step downward.
            step=zeros(1,size(Intensity,2));
            step(eadiff2==-2)=1;
            step(eadiff2==2)=-1;
            step=cumsum(step);
            step=step-min(step);
            %if size(pospeak)>0;for i=1:size(pospeak,2);line([pospeak(i) pospeak(i)], [0 400],'color','cyan');end;end
            %if size(negpeak)>0;for i=1:size(negpeak,2);line([negpeak(i) negpeak(i)], [0 -400],'color','red');end;end
            %addaxis(X_axis,step,[0,max(step)+1],'-m')
            %ylim([-200 800])
            hold off
            
            %set the number of molecules to be the maximal number in the
            %stepwise created graph
            numbers(k)=max(step);
            disp(['max edge steps:' num2str(max(step))]);
            pause()
            
            if numbers(k)>0; labelledplasmids=labelledplasmids+1;end
            plasmids=plasmids+1;
        end
    end
end
close(h)

labels_sum(1:10)=0;
for i=1:max(numbers)
    labels_sum(i)=sum(numbers(:)==i-1);
end
disp('')
disp(['Total plasmids: ' num2str(plasmids)])
disp(['no labels: ' num2str(plasmids-labelledplasmids) ' (' num2str(100*(1-labelledplasmids/plasmids)) '%)'])
disp(['1 label: ' num2str(labels_sum(2)) ' (' num2str(100*labels_sum(2)/plasmids) '%)'])
totallabels=labels_sum(2);
for i = 3:size(labels_sum,2)
    if labels_sum(i)>0
        disp([num2str(i-1) ' labels: ' num2str(labels_sum(i)) ' (' num2str(100*labels_sum(i)/plasmids) '%)'])
    end
    totallabels=totallabels+(i-1)*labels_sum(i);
end

function intensity()
Counter=findobj('Tag','Counter');
edit_circularity=findobj('tag','edit_circularity');
maxcircularity=str2double(get(edit_circularity,'String'));
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));
edit_expansion=findobj(Counter,'Tag','edit_expansion');
expansion=str2double(get(edit_expansion,'string'));
edit_horizontal=findobj(Counter,'Tag','edit_horizontal');
edit_vertical=findobj(Counter,'Tag','edit_vertical');
horizontal=str2double(get(edit_horizontal,'string'));
vertical=str2double(get(edit_vertical,'string'));
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));
edit_bins=findobj(Counter,'Tag','edit_bins');
bins=str2double(get(edit_bins,'string'));

Data=get(Counter,'Userdata');
%load the blackandwhite image
[imgBW,img1]=Blackandwhite(Data);
imgBW2 = bwareaopen(imgBW, minarea); %ignore smaller then minarea pixels
imsize=[size(imgBW,1),size(imgBW,2)];
imgBW2=~imgBW2;
magy=round(imsize(1)/Data.yaxis);
magx=round(imsize(2)/Data.xaxis);


%Load the bleaching image
fn3=Data.fn3;
fp3=Data.fp3;

bleaching_img=imread([fp3 fn3],1);

%small piece of code to average first .. frames.
maxframes=20;
tempimg=double(zeros(size(bleaching_img,1),size(bleaching_img,2)));
for x=1:maxframes;
    tempimg=tempimg+double(imread([fp3 fn3],x));
end
bleaching_img=tempimg./maxframes;


fr=length(imfinfo([fp3 fn3]));

bleaching_img=flipdim(bleaching_img,1);
% ./attenuate;
% maxframe=5;
% for i=2:maxframe
%     img=imread([fp3 fn3],i);
%     img=flipdim(img,1);
%     bleaching_img=bleaching_img+img;
% end
% bleaching_img=bleaching_img./maxframe;

%determine the background using a median of the figure
background=median(median(bleaching_img));

%if there are locations loaded
if isfield(Data,'fp2')
    %transform the locations so that they overlap with the BWimage
    [X,Y,pos]=load_gaussian(Data,imsize);

    if ~isempty(pos)
        pos(:,3)=pos(:,3)-0.5+vertical;%y correction  %REMOVE up+ down-
        pos(:,2)=pos(:,2)-0.5+horizontal;%x correction  %REMOVE left- right+
        Xpos=pos(:,2)*magx; %assuming 2048 pixel in img1 and 512 in original, mag=4
        Ypos=size(img1,1)-pos(:,3)*magy; %Y-axis is reversed! 
    end
    allin=ones([length(X),1]);
else
    fn=0;
end


%If the image is zoomed there is a data field x_box (and y_box)
%indicating the zoomed field, cut both the high res image and the black
%and white image
if isfield(Data,'x_box')
        y_box=Data.y_box;
        x_box=Data.x_box;
        
        if isfield(Data,'fp2')
            X=X-min(x_box)*magx;
            Y=Y-min(y_box)*magy;
        end
        img1=img1(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
        imgBW2=imgBW2(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
        
        bleaching_img=bleaching_img(min(y_box):max(y_box),min(x_box):max(x_box));
end

%calculate the boundaries of BW image
[B,L] = bwboundaries(imgBW2);
%calculate the area of the regions

stats = regionprops(L,'Area','Centroid');

%initialize some vectors
numbers=[];
if size(B)>0
    numbers(length(B))=0;
else
    numbers=[];
end
plasmids=0;
labelledplasmids=0;
circularity=ones(length(B),1).*-1; %initialize to -1
circularity(1)=-1;
area=zeros(length(B),1);
perimeter=zeros(length(B),1);

%plot the black and white image
figure(1);imshow(imgBW2)

%plot the high resolution image
mx=0.6*max(max(img1));
mn=min(min(img1));
figure(2);imshow(img1,[mn mx])
hold on
text(0,0,strcat('\color{yellow}BALM image'))
hold off

%plot the bleaching image
mx=max(max(bleaching_img));
mn=min(min(bleaching_img));
figure(4);imagesc(bleaching_img,[mn mx]);colormap('gray')
hold on
text(0,0,strcat('\color{yellow}Bleaching image'))
hold off
x=1;

for k = 2:length(B) %first boundary is complete image
    boundary = B{k};
    area(k) = stats(k).Area;
 
    %calculate the perimeter
    delta_sq = diff(boundary).^2; 
    perimeter(k) = sum(sqrt(sum(delta_sq,2)));
    
    %Check if the perimeter is bigger than 0 (otherwise there is no
    %area value) and the area is bigger than the minimal area
    if perimeter(k)>0 && area(k)>minarea
        %Calculate the circularity
        circularity(k)=perimeter(k).^2/(4*pi*area(k));
        %Check if circularity is smaller than set circularity
        if circularity(k)<maxcircularity
            xboundary=boundary(:,2);yboundary=boundary(:,1);
            %Increase the boundaries of the spots (This currently only works for circular spots,
            %might need to change this)
            [Xboundary,Yboundary]=increaseboundaries(xboundary,yboundary,expansion); 
            Xboundary2=(Xboundary/magy)+horizontal; Yboundary2=(Yboundary/magx)+vertical;
            
            %create a mask for the bleaching image
            mask = roipoly(bleaching_img,Xboundary2,Yboundary2);
            
            %plot the locations on the bleaching figure and the high
            %resolution image

            figure(4)
            hold on 
            plot(Xboundary2, Yboundary2, 'r', 'LineWidth', 0.2)
            hold off
            
            %calculate the intensity within the region, subtract the
            %background (should be 0 when no molecules)
            Intensity(x)=mean(bleaching_img(mask))-background;
            x=x+1;
            figure(2)
            hold on
            plot(Xboundary, Yboundary, 'r', 'LineWidth', 0.2)
            
            %If there is a file loaded with locations check if there are
            %molecules within the boundaries (normal count method)
            if isfield(Data,'fn2')
                in=inpolygon(X,Y,Xboundary,Yboundary);
                plot(X(in>0),Y(in>0),'mx','markersize',6);
                if ~isempty(pos)
                    inpos=inpolygon(Xpos,Ypos,Xboundary,Yboundary);
                    labels=sum(in)-sum(inpos);
                    plot(Xpos(inpos>0),Ypos(inpos>0),'gx','markersize',6);
                else
                    labels=sum(in);
                end
                allin=allin-in;
                numbers(k)=labels;
                if labels>0; labelledplasmids=labelledplasmids+1;end
                plasmids=plasmids+1;
            end
            hold off
        end
    end
end


%create histogram of intensity using bins
figure(3);hist(double(Intensity),bins)
[nelements,xcenters]=hist(double(Intensity),bins);
total=[xcenters',nelements'];

%Save the histogram
save('temp3.mat', 'nelements','xcenters','total');

if isfield(Data,'fn2')
    %indicate the total |Boundaries found
    figure(2)

    text(30,30,strcat('\color{yellow}Objects Found:',num2str(length(B))))

    %plot the locations that are not found (allin=1) inside a plasmid 
    plot(X(allin>0),Y(allin>0),'bx','markersize',6);
    hold off

    %initialize vector containing 
    labels_sum(1:10)=0;
    for i=1:max(numbers)+1
        labels_sum(i)=sum(numbers(:)==i-1);
    end

    %for saving purposes
    C=labels_sum';

    %display results
    disp('')
    disp(['Total plasmids: ' num2str(plasmids)])
    disp(['no labels: ' num2str(plasmids-labelledplasmids) ' (' num2str(100*(1-labelledplasmids/plasmids)) '%)'])
    disp(['1 label: ' num2str(labels_sum(2)) ' (' num2str(100*labels_sum(2)/plasmids) '%)'])
    totallabels=labels_sum(2);
    for i = 3:size(labels_sum,2)
        if labels_sum(i)>0
            disp([num2str(i-1) ' labels: ' num2str(labels_sum(i)) ' (' num2str(100*labels_sum(i)/plasmids) '%)'])
        end
        totallabels=totallabels+(i-1)*labels_sum(i);
    end
    freedye=size(allin,1)-totallabels;
    disp(['free dye: ' num2str(freedye)]);

    C=[freedye;plasmids; plasmids-labelledplasmids;  C(2:end)];

    %save the file with the total number of locations per spot (first entry is
    %number of free dyes, the 2nd entry is the number of plasmids, 3rd entry is
    %number of plasmids without labels, then the plasmids with labels in order
    save('temp2.mat', 'C');
end


function areahistogram()
Counter=findobj('Tag','Counter');
Data=get(Counter,'Userdata');
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));

%create black and white image from data
[imgBW,~]=Blackandwhite(Data);
imgBW2 =~imgBW;
imgBW=imgBW2;

%Calculate the boundaries
[B,L] = bwboundaries(imgBW2);

%Obtain area stats from the regions L 
stats = regionprops(L,'Area','Centroid');

%if the minimal area is set
if ~isempty(minarea)
    %ignore smaller then minarea pixels
    imgBW = bwareaopen(imgBW, minarea); 
    
    %Display of spots in a figure
    %This trick allows us to see the ignored spots (intensity 0.5) and the
    %included spots (intensity 1) over the background (intensity 0)
    both=single(imgBW2+imgBW);   
    figure;imshow(both/2);%,[1 1 1; 1 0 0]);% 0 1 0])
    colormap('hot')
else
    imshow(~imgBW)
end
%Loop through all included boundaries and store each area
area=zeros(size(B),1);
for k = 2:length(B) %first boundary is complete image
    area(k) = stats(k).Area;
end

%Make a histogram of all the stored areas
x=1:2:max(area);
figure;hist(area,x)
figure;hist(area(area<100),x(x<100))

function circularity_histogram()
Counter=findobj('Tag','Counter');
Data=get(Counter,'Userdata');

%create black and white image from data
[imgBW,~]=Blackandwhite(Data);

%Calculate the boundaries
[B,L] = bwboundaries(imgBW2);

%Obtain area stats from the regions L 
stats = regionprops(L,'Area','Centroid');

%plot image
figure;imshow(imgBW)

%initialize some vectors
area=zeros(size(B),1);
perimeter=zeros(size(B),1);
circularity=ones(size(B),1).*-1;
circularity(1)=-1;

for k = 2:length(B) %first boundary is complete image, skip it
    %obtain k'th boundary
    boundary = B{k};
    %obtain area of k
    area(k) = stats(k).Area;
    
    %calculate perimeter
    delta_sq = diff(boundary).^2; 
    perimeter(k) = sum(sqrt(sum(delta_sq,2)));
    
    %if perimeter exceeds set value
    if perimeter(k)>0
        circularity(k)=perimeter(k).^2/(4*pi*area(k));
    end
end
x2=0:0.1:4;
figure;hist(circularity',x2)
xlim([0 4])

function test_expansion()
Counter=findobj('Tag','Counter');
edit_expansion=findobj(Counter,'Tag','edit_expansion');
expansion=str2double(get(edit_expansion,'string'));
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));
edit_circularity=findobj('tag','edit_circularity');
maxcircularity=str2double(get(edit_circularity,'String'));

Data=get(Counter,'Userdata');
%create black and white image from data
[imgBW,img1]=Blackandwhite(Data);
imgBW = bwareaopen(imgBW, minarea); %ignore smaller then minarea pixels


[B,L] = bwboundaries(~imgBW);
stats = regionprops(L,'Area','Centroid');

%display image
mx=200*max(max(img1));
mn=min(min(img1));
figure;imshow(img1,[mn mx])

%Hold the image and plot all the boundaries as well
hold on
for k = 2:length(B) %first boundary is complete image
    boundary = B{k};
    area = stats(k).Area;
    
    %calculate the perimeter
    delta_sq = diff(boundary).^2; 
    perimeter = sum(sqrt(sum(delta_sq,2)));
    
    %Check if the perimeter is bigger than 0 (otherwise there is no
    %area value) and the area is bigger than the minimal area
    if perimeter>0 && area>minarea
        %Calculate the circularity
        circularity=perimeter.^2/(4*pi*area);
        %Check if circularity is smaller than set circularity (note can
        %include a 0 check to disable this in case of non-circular
        %molecules)
        if circularity<maxcircularity
            xboundary=boundary(:,2);yboundary=boundary(:,1);
            %Increase the boundaries of the plasmids (This currently only works for circular plasmids,
            %might need to change this)
            [Xboundary,Yboundary]=increaseboundaries(xboundary,yboundary,expansion); 

            plot(Xboundary, Yboundary, 'r', 'LineWidth', 0.2)
        end
    end
end
hold off


function [Xboundary,Yboundary]=increaseboundaries(xboundary,yboundary,expansion)
%calculate mean values of the boundaries
m1=mean(xboundary);m2=mean(yboundary); % x and y are the cordiantes of present polygon

%subtract mean values from boundary
Xboundary=xboundary-m1; Yboundary=yboundary-m2;

%add or subtract the expansion value for x and y boundaries
Xboundary(Xboundary<0)=Xboundary(Xboundary<0)-expansion;Xboundary(Xboundary>0)=Xboundary(Xboundary>0)+expansion; %Xboundary=Xboundary.*expansion;Yboundary=Yboundary*expansion; % 1.2 expansion factor
Yboundary(Yboundary<0)=Yboundary(Yboundary<0)-expansion;Yboundary(Yboundary>0)=Yboundary(Yboundary>0)+expansion;

%add mean value back to ordinary boundary
Xboundary=Xboundary+m1; Yboundary=Yboundary+m2;


function [WT,S]=mal_fwt(J, x)

% J  = # of scales
% x  = data vector
% WT = matrix with waveleth transform
% S  = coarse low-pass time series remaining

N= length(x);
M=2*N;

%normalization coefficients

lambda = [1.5, 1.12, 1.03, 1.01];
if J>4
    lambda = [lambda, ones(1,J-4)];
end

%filter coefficients

H = [0.125, 0.375, 0.375, 0.125];
G = -1* [-2.0, 2.0];

%convolution offsets

Gn=2;
for j = 1:J-1
    znum=2^j-1;
    Gn=[Gn,((znum+1)/2)+1];
end
Hn=3;
for j=1:J-1
    znum=2^j-1;
    Hn=[Hn,((znum+1)/2)+znum+2];
end

%compute the WT at each scale
%signal is odd symmetric periodically extended for borders

S=[fliplr(x),x,fliplr(x)];
WT=[];
%figure(1)
for j=0:J-1
    
    znum=2^j-1; % # of zeros
    Gz=in_zeros(G,znum); % insert zeros into G
    Hz = in_zeros(H, znum); % insert zeros into H
    
% compute wavelet transform at scale j and store in WT

    Wf=(1/lambda(j+1))*conv(S,Gz); % size(Wf)
    Wf=Wf(N+Gn(j+1):2*N+Gn(j+1)-1);
    WT=[WT,Wf'];
    
% compute next time series

    S2=conv(S,Hz);
    S2=S2(N+Hn(j+1):2*N+Hn(j+1)-1);
    S=[fliplr(S2),S2,fliplr(S2)];
end

S=S(N+1:2*N);
return
function x=iwt(WT,S)

%Inverse wavelegt transform
%
% WT = matrix with wavelet transform
% S  = leftover low-pass time series
% x  = reconstructed time series

[N,J]=size(WT); %J=#of scales, N = data length

%normalization coefficients

lambda = [1.5, 1.12, 1.03, 1.01];
if J>4
    lambda = [lambda, ones(1,J-4)];
end

%filter coefficients

H = [0.125, 0.375, 0.375, 0.125];
K=[0.0078125, 0.054785, 0.171875];
K=[K, -1*fliplr(K)];

% convolution offsets

Kn=3;
for j=1:J-1
    znum = 2^j-1;
    Kn=[Kn, ((znum+1)/2)+2*znum+3];
end
Hn=2;
for j=1:J-1
    znum2^j-1;
    Hn=[Hn,((znum+1)/2)+znum+2];
end

% recursively compute the inverse WT, proceeding down in scales, signal is
% odd-symmetric periodically extended

S=S(:);
S1=[fliplr(S),S,fliplr(S)]; S1=S1(:);
for j=J:-1:1
    
    znum=2^(j-1)-1; % # of zeros
    Kz = in_zeros(K,znum);% insert zeros into K
    Hz=in_zeros(H,znum);% insert zeros into H
    
    WTj = WT(:,j); WTj=WTj(:);
    WT_ext=[fliplr(WTj),WTj,fliplr(WTj)]; WT_ext=WT_ext(:);
    A1=lambda(j)*conv(Kz,WT_ext);
    A1=A1(N+Kn(j):2*N+Kn(j)-1);
    A2=conv(Hz,S1);
    A2=A2(N+Hn(j):2*N+Hn(j)-1);
    
    S1=A1+A2;
    S1=[fliplr(S1)',S1',fliplr(S1)'];S1=S1(:);
    
end %end IWT loop

x=S1(N+1:2*N)';

return

%y=ins_zeros(x,n)
%
% Insert n zeros between elements of vector x
% and return in y. Used in discrete wavelet transfor.
%
function y=in_zeros(x,n)

if n==0
    y=x;
    return
end

if n>0
    newlen=(n+1)*length(x); %length of y
    y=zeros(1,newlen); %new filter vector
    index=1:n+1:newlen-n; % indices of data
    y(index)=x; %insert data into y
end

return

function [X,Y,pos]=load_gaussian(Data,imsize)
Counter=findobj('Tag','Counter');
edit_horizontal=findobj(Counter,'Tag','edit_horizontal');
edit_vertical=findobj(Counter,'Tag','edit_vertical');
horizontal=str2double(get(edit_horizontal,'string'));
vertical=str2double(get(edit_vertical,'string'));

%calculate the magnification
magy=round(imsize(1)/Data.yaxis);
magx=round(imsize(2)/Data.xaxis);

%the filepath where the locations are stored
fp2=Data.fp2;
fn2=Data.fn2;

pos=[];
%If the file is a text, the emitters are located with igor
if fn2(end-2:end)=='txt' 
    igData=importdata([ fp2 fn2]);

    gaussians(:,1)=igData.data(2:end,1); %frame
    gaussians(:,2)=igData.data(2:end,4)+1; %x-location
    gaussians(:,3)=igData.data(2:end,5)+1; %y-location
    gaussians(:,4)=igData.data(2:end,3); %xwidth gaussian
    gaussians(:,5)=igData.data(2:end,3); %ywidth gaussian
else %otherwise the emitters are located with matlab
    S=load([fp2 fn2]);
    gaussians=S.gaussians;
    
    %small inclusion in case of negative fitting.
    if isfield(S.flt,'positivefit') && S.flt.positivefit==1
        pos=S.positivegaussians;
    end
end

%correct the gaussians due to drift, set by the edit_horizontal and
%edit_vertical field
gaussians(:,3)=gaussians(:,3)-0.5+vertical;%y correction  %REMOVE up+ down-
gaussians(:,2)=gaussians(:,2)-0.5+horizontal;%x correction  %REMOVE left- right+

%Convert the gaussians to X and Y: magnified to cover the full high
%resolution image (1 pixel transformed to mag pixels)
X=gaussians(:,2)*magx; %amplify the x and y coordinates (default mag=4)
Y=imsize(1)-gaussians(:,3)*magy; %Y-axis is reversed! 

%if there are negative fits include these spots as well, saved in pos, same
%steps as gaussians
if ~isempty(pos)
    pos(:,3)=imsize(1)-magy*(pos(:,3)-0.5+vertical);%y correction  %REMOVE up+ down-
    pos(:,2)=magx*(pos(:,2)-0.5+horizontal);%x correction  %REMOVE left- right+
end