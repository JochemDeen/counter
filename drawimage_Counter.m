function drawimage_Counter
Counter=findobj('Tag','Counter');
Data=get(Counter,'UserData');
axes1=findobj(Counter,'tag','axes1');
axes2=findobj(Counter,'tag','axes2');
axes3=findobj(Counter,'tag','axes3');

%names for edits
edit_circularity=findobj('tag','edit_circularity');
maxcircularity=str2double(get(edit_circularity,'String'));
edit_minarea=findobj(Counter,'tag','edit_minarea');
minarea=str2double(get(edit_minarea,'String'));
edit_threshold=findobj(Counter,'Tag','edit_threshold');
threshold=str2double(get(edit_threshold,'string'));
edit_expansion=findobj(Counter,'Tag','edit_expansion');
expansion=str2double(get(edit_expansion,'string'));
slider_threshold=findobj(Counter,'Tag','slider_threshold');
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));
edit_horizontal=findobj(Counter,'Tag','edit_horizontal');
edit_vertical=findobj(Counter,'Tag','edit_vertical');

%Disable edit areas
set(edit_minarea,'enable','off')
set(edit_threshold,'enable','off')
set(slider_threshold,'enable','off')
set(edit_expansion,'enable','off')
set(edit_circularity,'enable','off')
set(edit_attenuate,'enable','off')
set(edit_horizontal,'enable','off')
set(edit_vertical,'enable','off')
pause(0.01)



if Data.CCDpath>0 & ~isempty(minarea) & ~isempty(threshold)
    
    %Read the high resolution image of plasmids
    img1=imread([Data.CCDpath Data.CCDname]);
    
    %Flip the image 90 degrees
    img1=flipdim(img1,1);%./25;
    
    %Convert image to single
    img1=single(img1);
    %img1=uint16(img1);
    %imgBW=im2bw(img1./1500000,threshold);
    
    %Create black and white image using threshhold. To cover the full range
    %a dirty trick is used to attenuate the image (default value 1)
    
    %load the blackandwhite image
    [imgBW,img1]=Blackandwhite(Data);

    imgBW2 = bwareaopen(imgBW, minarea); %ignore smaller then minarea pixels
%     imgBW=~imgBW;
%     imgBW2=~imgBW2;

    %If the image is zoomed there is a data field x_box (and y_box)
    %indicating the zoomed field, cut both the high res image and the black
    %and white image
    if isfield(Data,'x_box')
        magy=round(size(img1,1)/Data.yaxis);
        magx=round(size(img1,2)/Data.xaxis);
        y_box=Data.y_box;
        x_box=Data.x_box;
        
        img1=img1(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
        imgBW2=imgBW2(min(y_box)*magy:max(y_box)*magy,min(x_box)*magx:max(x_box)*magx);
    end

    %plot the black and white image
    axes(axes2);cla;
    imagesc(imgBW2);
    axis image;axis off;
    set(axes2,'tag','axes2')
    
    %Create boundaries from the black and white image
    [B,L] = bwboundaries(~imgBW2);
    stats = regionprops(L,'Area','Centroid');
    
    %Plot the high resolution image and overlay (for-loop) the obtained
    %boundaries if the boundaries conform to the set values in the rest of
    %the program.
    mx=max(max(img1));
    mn=min(min(img1));
    axes(axes1);cla;
    imagesc(img1,[mn mx]);
    axis image;axis off;colormap(gray)
    hold on
    %Loop through all boundaries
    for k = 2:length(B) %first boundary is complete image, so ignore that one
        boundary = B{k};
        area(k) = stats(k).Area;

        delta_sq = diff(boundary).^2; 
        perimeter(k) = sum(sqrt(sum(delta_sq,2)));
        
        %Check if the perimeter is bigger than 0 (otherwise there is no
        %area value) and the area is bigger than the minimal area
        if perimeter(k)>0 && area(k)>minarea
            %Calculate the circularity
            circularity(k)=perimeter(k).^2/(4*pi*area(k));
            %Check if circularity is smaller than set circularity (note can
            %include a 0 check to disable this in case of non-circular
            %molecules)
            if circularity(k)<maxcircularity
                %small piece of code to increase the boundaries of the
                %plasmids (This currently only works for circular plasmids,
                %might need to change this)
                xboundary=boundary(:,2);yboundary=boundary(:,1);
                m1=mean(xboundary);m2=mean(yboundary); % x and y are the cordiantes of present polygon
                Xboundary=xboundary-m1; Yboundary=yboundary-m2;
                Xboundary(Xboundary<0)=Xboundary(Xboundary<0)-expansion;Xboundary(Xboundary>0)=Xboundary(Xboundary>0)+expansion; %Xboundary=Xboundary.*expansion;Yboundary=Yboundary*expansion; % 1.2 expansion factor
                Yboundary(Yboundary<0)=Yboundary(Yboundary<0)-expansion;Yboundary(Yboundary>0)=Yboundary(Yboundary>0)+expansion;
                Xboundary=Xboundary+m1; Yboundary=Yboundary+m2;
                
                %plot boundaries
                plot(Xboundary, Yboundary, 'r', 'LineWidth', 0.2)
            end
        end
    end
    hold off
    set(axes1,'tag','axes1')

end

%If there is a loaded file with localized emitters (fp2)
if isfield(Data,'fp2')
    
    %checks if the file is a text file (which indicates it is IGOR based)
    if Data.fn2(end-2:end)=='txt'
        igData=importdata([Data.fp2 Data.fn2]);

        gaussians(:,1)=igData.data(2:end,1); %frame
        gaussians(:,2)=igData.data(2:end,4)+1; %x-location (starts from 0, thus increase by 1)
        gaussians(:,3)=igData.data(2:end,5)+1; %y-location (starts from 0, thus increase by 1)
        gaussians(:,4)=igData.data(2:end,3); %xwidth gaussian
        gaussians(:,5)=igData.data(2:end,3); %ywidth gaussian
        
        %Because there is no image associated with the igor images, assume
        %that it is 4 times smaller than the black and white image
        Data.xaxis=size(imgBW2,2)/4;
        Data.yaxis=size(imgBW2,1)/4;
        CCDpath2=0;
        %[CCDname2, CCDpath2]=uigetfile({'*.TIF;*.tif','Tif image (*.TIF,*.tif)'},'select image file');
        if CCDpath2>0
            img1=imread([CCDpath2 CCDname2],1);
            Data.CCDpath2=CCDpath2;
            Data.CCDname2=CCDname2;
        else
            %create black image as the background image for hte localized spots because the igor-text file has no image
            %associated with the file
            img1=zeros(Data.yaxis,Data.xaxis);
            img1(1,1)=1;
        end
        
        set(Counter,'UserData',Data);
    else %if the file is not a text file it has to be a .m file (matlab localization based)
        S=load([Data.fp2 Data.fn2]);
        img1=imread([Data.CCDpath2 Data.CCDname2],1);
        gaussians=S.gaussians;

       if exist(Data.CCDpath2, 'dir'); %This is the directory where the image is stored
        %apparently nothing happens
       else
            %if the directory does not exist, give the option to load
            %another TIF file to act as the background for the localized
            %image
            [CCDname2, CCDpath2_b]=uigetfile({'*.TIF;*.tif','Tif image (*.TIF,*.tif)'},'select image file');
            if CCDpath2_b>0
                CCDpath2=CCDpath2_b;
                img1=imread([CCDpath2 CCDname2],1);
                Data.CCDpath2=CCDpath2;
                Data.CCDname2=CCDname2;
                set(Counter,'UserData',Data);
            end
       end
    end
    
    %flip image 9 degrees
    img1=flipdim(img1,1);
    X=gaussians(:,2);
    %when plotting images, y axis runs from 512 (bottom) to 0 (top), the
    %reverse of a normal plot, so to accomodate this the values need to be
    %reversed.
    Y=Data.yaxis-gaussians(:,3);
    
    %plot background image        
    axes(axes3);cla
    imagesc(img1);
    axis image; axis off; colormap(gray)
    hold on
    
    %Plot locations
    plot(X,Y,'rx','markersize',2)
    
    %If the image is zoomed there is a data field x_box (and y_box)
    %indicating the zoomed field. plot the full background image and plot
    %the lines indicating where the image is zoomed
    if isfield(Data,'x_box')
        x=Data.x_box;
        y=Data.y_box;
        line([min(x) min(x)],[min(y) max(y)],'color','green')
        line([min(x) max(x)],[max(y) max(y)],'color','green')
        line([max(x) max(x)],[max(y) min(y)],'color','green')
        line([max(x) min(x)],[min(y) min(y)],'color','green')
    end

    
    hold off
    set(axes3,'Tag','axes3');
elseif isfield(Data,'fp3')%If the bleaching image is independantly loaded it is stored under "fp3", if no locations are loaded this image will be plotted instead
    %load the bleaching image
    img1=imread([Data.fp3 Data.fn3],1);
    %rotate 90 degrees
    img1=flipdim(img1,1);
    
    %plot the image
    axes(axes3);cla
    imagesc(img1);
    axis image; axis off; colormap(gray)
        set(axes3,'Tag','axes3');
end

    
%Enable edit areas    
set(edit_minarea,'enable','on')
set(edit_threshold,'enable','on')
set(slider_threshold,'enable','on')
set(edit_expansion,'enable','on')
set(edit_circularity,'enable','on')
set(edit_attenuate,'enable','on')
set(edit_horizontal,'enable','on')
set(edit_vertical,'enable','on')
pause(0.01)