function [imgBW2,img1]=Blackandwhite(Data)
Counter=findobj('Tag','Counter');
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));
edit_threshold=findobj(Counter,'Tag','edit_threshold');
threshold=str2double(get(edit_threshold,'string'));

CCDname=Data.CCDname;
CCDpath=Data.CCDpath;
img1=imread([CCDpath CCDname]);
img1=uint16(img1);
img1=flipdim(img1,1)./attenuate;

%estimate background with disk shapes structures of size 50
background = imopen(img1,strel('disk',50));

%subtract background from image
img1=img1-background;

%adjust image:
%Use imadjust to increase the contrast of the processed image I2 by saturating 1% of the data at both low and high intensities and by stretching the intensity values to fill the uint8 dynamic range.
%img1=imadjust(img1);

imgBW=im2bw(img1,threshold);
imgBW2=~imgBW;