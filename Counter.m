function varargout = Counter(varargin)

%

% Last Modified by GUIDE v2.5 16-Jul-2015 10:55:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Counter_OpeningFcn, ...
                   'gui_OutputFcn',  @Counter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Counter is made visible.
function Counter_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;



guidata(hObject, handles);

% UIWAIT makes Counter wait for user response (see UIRESUME)
% uiwait(handles.Counter);
% --- Outputs from this function are returned to the command line.
function varargout = Counter_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;


% These functions set the values for the imaging and boundary conditions
function edit_minarea_Callback(hObject, eventdata, handles)
%Set minimal area and redraw axes
drawimage_Counter 
function edit_circularity_Callback(hObject, eventdata, handles)
%Set minimal circularity and redraw axes
drawimage_Counter
function edit_xaxis_Callback(hObject, eventdata, handles)
function edit_threshold_Callback(hObject, eventdata, handles)
%Set threshold, redraw the slider and redraw axes
Counter=findobj('Tag','Counter');
edit_threshold=findobj(Counter,'Tag','edit_threshold');
threshold=str2num(get(edit_threshold,'string'));
slider_threshold=findobj(Counter,'Tag','slider_threshold');


set(slider_threshold,'value',threshold)
set(slider_threshold,'min',0.1*threshold)
set(slider_threshold,'SliderStep',[0.1*threshold,0.5*threshold])
set(slider_threshold,'max',10*threshold)
        
drawimage_Counter
function edit_expansion_Callback(hObject, eventdata, handles)
%Set expansion of boundaries and redraw axes
drawimage_Counter
function edit_attenuate_Callback(hObject, eventdata, handles)
%Set attenuation and redraw axes
drawimage_Counter
function edit_bins_Callback(hObject, eventdata, handles)
%Set number of bins and redraw axes
set(hObject,'enable', 'off')
drawimage_Counter
set(hObject,'enable', 'on')

%These values test some stored values
function pushbutton_graythresh_Callback(hObject, eventdata, handles)
%This function uses the graythresh function to calculate the threshold used
%for creating the black and white image
Counter=findobj('Tag','Counter');
edit_threshold=findobj(Counter,'Tag','edit_threshold');
Data=get(Counter,'Userdata');
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));

%Load the high resolution image pathway from the userdata
CCDname=Data.CCDname;
CCDpath=Data.CCDpath;

%load the image
img1=imread([CCDpath CCDname]);

%img1=flipdim(img1,1)./max(max(img1));
%flip the image and convert to single
img1=uint16(flipdim(img1,1));
%use graythresh to calculate the threshold and store it in the edit field
set(edit_threshold,'string',num2str(graythresh(img1)));%+0.5/X));
%creates the black and white image resulting from using this threshold,
%also displays it in a new figure
imgBW=im2bw(img1./attenuate,graythresh(img1));
figure;imshow(imgBW);
function pushbutton_area_histogram_Callback(hObject, eventdata, handles)
%displays a histogram of all found areas
Overlap_function('areahistogram')
function pushbutton_circ_histogram_Callback(hObject, eventdata, handles)
%displays a histogram of the circularity of all the found spots
Overlap_function('circularity_histogram')
function pushbutton_test_Callback(hObject, eventdata, handles)
%tests the threshold image
Counter=findobj('Tag','Counter');
edit_threshold=findobj(Counter,'Tag','edit_threshold');
threshold=str2num(get(edit_threshold,'string'));
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));

%Load the high resolution image
Data=get(Counter,'Userdata');
CCDname=Data.CCDname;
CCDpath=Data.CCDpath;
img1=imread([CCDpath CCDname]);

%rotate 90degrees
img1=flipdim(img1,1);%./25;

%convert to single
img1=single(img1);

%display image
mx=0.6*max(max(img1));
mn=min(min(img1));
figure;imshow(img1,[mn mx])
%figure;imshow(img1(img1(:)>max(max(img1))))
img2=img1./attenuate;

%create black and white image
[imgBW]=Blackandwhite(Data);

%plot black and white image
figure;imshow(imgBW);
function pushbutton_expansion_Callback(hObject, eventdata, handles)
%plots an image of the black and white image and its incresed boundaries.
Overlap_function('test_expansion')
function pushbutton_zoom_Callback(hObject, eventdata, handles)
%Function to zoom in certain region of both the high resolution image and
%the localized emitters. The zoom in happens on the bleaching image loaded
%with the localized emitters
Counter=findobj('Tag','Counter');
Data=get(Counter,'Userdata');

%If a bleaching image is loaded accomponying the localized emitters
if isfield(Data,'fp2')
    %load the image
    img1=imread([Data.CCDpath2 Data.CCDname2],1);
elseif isfield(Data,'fp3') %else perhaps a bleaching image is loaded without the localized emitters
    %load the image
    img1=imread([Data.fp3 Data.fn3],1);
else
    %nothing
end

if isfield(Data,'fp2') || isfield(Data,'fp3')
    %rotate the image 90 degrees
    img1=flipdim(img1,1);
    
    %check the size of the image
    xaxis=size(img1,2);
    yaxis=size(img1,1);
    
    s=[1,yaxis,1,xaxis];
    
    %first mouseinput
    [gx1, gy1, op]=ginput(1);

    %if left-click
    if op<2
        %check if the location does not exceed image boundaries
        if gx1+s(3)-1<s(3); gx1=s(3);
        elseif gx1+s(3)-1>s(4); gx1=s(4);
        elseif gy1+s(1)-1<s(1); gy1=s(1);
        elseif gy1+s(1)-1>s(2); gy1=s(2); end
        
        %draw a line
        line(gx1, 1:s(2)-s(1), 'Color','r');
        line(1:s(4)-s(3), gy1, 'Color','r');
        
        %hold and wait for the second input
        hold on;
        
        %second mouseinput
        [gx2, gy2, op]=ginput(1);
        line(gx2, 1:s(2)-s(1), 'Color','r');
        line(1:s(4)-s(3), gy2, 'Color','r');
        
        %if left-click and locations do not overlap
        if op<2 &gx1~=gx2 &gy1~=gy2
            %check if the location does not exceed image boundaries
            if gx2+s(3)-1<s(3); gx2=s(3);
            elseif gx2+s(3)-1>s(4); gx2=s(4);
            elseif gy2+s(1)-1<s(1); gy2=s(1);
            elseif gy2+s(1)-1>s(2); gy2=s(2); end
            if gx1<gx2; x=[gx1 gx2];else x=[gx2, gx1];end
            if gy1<gy2; y=[gy1 gy2];else y=[gy2, gy1];end
        end
        
        %set and store the zoom values in userdata
        Data.x_box=round(x);
        Data.y_box=round(y);
        set(Counter,'Userdata',Data)
    end
end
drawimage_Counter
function slider_threshold_Callback(hObject, eventdata, handles)
%this function reads the value from the slider and sets the threshold accordingly,
%redraws the images
Counter=findobj('Tag','Counter');
edit_threshold=findobj(Counter,'Tag','edit_threshold');
slider_threshold=findobj(Counter,'Tag','slider_threshold');
threshhold=get(slider_threshold,'Value');
set(edit_threshold,'string',num2str(threshhold))
        
drawimage_Counter
function pushbutton_resetzoom_Callback(hObject, eventdata, handles)
Counter=findobj('Tag','Counter');
Data=get(Counter,'Userdata');

%remove the fields x_box and y_box, resets the zoom
if isfield(Data,'x_box')
    Data=rmfield(Data,'x_box');
    Data=rmfield(Data,'y_box');
end

set(Counter,'Userdata',Data)
drawimage_Counter
function pushbutton_histogram_Callback(hObject, eventdata, handles)
Counter=findobj('Tag','Counter');
edit_threshold=findobj(Counter,'Tag','edit_threshold');
threshold=str2num(get(edit_threshold,'string'));
edit_attenuate=findobj(Counter,'Tag','edit_attenuate');
attenuate=str2double(get(edit_attenuate,'string'));


Data=get(Counter,'Userdata');

CCDname=Data.CCDname;
CCDpath=Data.CCDpath;
img1=imread([CCDpath CCDname]);
img1=flipdim(img1,1);

imgintensity=img1./attenuate;
figure;hist(imgintensity(:)',0:0.001:1)%0:0.01:1.1)
xlim([threshold 1])

%These functions load the data files
function pushbutton_load_Callback(hObject, eventdata, handles)
%This function loads the high resolution image and turns some initially
%disabled buttons on (including the load localization button)
Counter=findobj('Tag','Counter');
pushbutton_igor=findobj(Counter,'Tag','pushbutton_igor');
pushbutton_circ_histogram=findobj(Counter,'Tag','pushbutton_circ_histogram');
pushbutton_area_histogram=findobj(Counter,'Tag','pushbutton_area_histogram');
pushbutton_matlab=findobj(Counter,'Tag','pushbutton_matlab');
slider_threshold=findobj(Counter,'Tag','slider_threshold');
text4=findobj(Counter,'Tag','text4');
edit_threshold=findobj(Counter,'Tag','edit_threshold');

%Load the high resolution image
[CCDname, CCDpath]=uigetfile('*.TIF','select image file');
if CCDpath>0
    cd(CCDpath)
    
    %Load userdata
    Data=get(Counter,'UserData');
    
    %Set new high resolution path and CCD name
    Data.CCDname=CCDname;
    Data.CCDpath=CCDpath;
    
    %enable buttons
    set(pushbutton_igor,'enable','on')
    set(pushbutton_matlab,'enable','on')
    set(pushbutton_area_histogram,'enable','on')
    set(pushbutton_circ_histogram,'enable','on')
    
    %display path
    set(text4,'string',[CCDpath CCDname])
    
    %If there is a previous zoom setting present reset the zoom field (remove field)
    if isfield(Data,'x_box')
        Data=rmfield(Data,'x_box');
        Data=rmfield(Data,'y_box');
    end

    %Save userdata
    set(Counter,'UserData',Data)
    
    %Set the threshold
    threshold=str2num(get(edit_threshold,'string'));
    if isempty(threshold) %if no threshold previously set, create one using graythresh
        
        %load the high resolution image
        img1=imread([CCDpath CCDname]);
        img1=img1;
        X=max(max(img1));
        
        %flip the image and convert to single
        img1=uint16(flipdim(img1,1));
        img1=single(img1);
        %set(edit_threshold,'string',num2str(graythresh(img1)+0.5/X));
        
        %create threshold using graythresh function
        threshold=graythresh(img1);
        %store threshold in edit field
        set(edit_threshold,'string',num2str(threshold));
    end
    %Set the sliders
        set(slider_threshold,'value',threshold)
        set(slider_threshold,'min',0.1*threshold)
        set(slider_threshold,'SliderStep',[0.1*threshold,0.5*threshold])
        set(slider_threshold,'max',10*threshold)
    
end
%Draw image
drawimage_Counter
function pushbutton_igor_Callback(hObject, eventdata, handles)
%This function loads the igor localized emitters (stored in a text file)
Counter=findobj('Tag','Counter');
pushbutton_Go=findobj(Counter,'Tag','pushbutton_Go');

text5=findobj(Counter,'Tag','text5');
[fn2, fp2]=uigetfile('*.txt','select Igor data file');
if fp2>0
    cd(fp2)
    
    %Get userdata
    Data=get(Counter,'UserData');
    
    %store the selected text file as fn2 and fp2 in userdata
    Data.fn2=fn2;
    Data.fp2=fp2;
    set(pushbutton_Go,'enable','on')
    
    %Display the path and file of the loaded igor text
    set(text5,'string',[fp2 fn2])
    %Store userdata
    set(Counter,'UserData',Data)
end
%redraw image
drawimage_Counter
function pushbutton_matlab_Callback(hObject, eventdata, handles)
%This function loads the matlab localized emitters (stored in a .mat file)
%and loads the associated CCD image (stored in the .m file as CCDpath and
%CCDname

Counter=findobj('Tag','Counter');
pushbutton_Go=findobj(Counter,'Tag','pushbutton_Go');
text5=findobj(Counter,'Tag','text5');

%loads the filename
[fn2, fp2]=uigetfile('*.mat','select Matlab data file');
if fp2>0
    cd(fp2)
    %load userdata
    Data=get(Counter,'UserData');
    
    %store the loaded filename containing the localized emitters as fn2 and
    %fp2 in userdata
    Data.fn2=fn2;
    Data.fp2=fp2;
    %load the matlab file
    S=load([Data.fp2 Data.fn2]);
    %Store the filepath containing the bleaching image from the matlab file in userdata
    Data.CCDpath2=S.CCDpath;
    if ~strcmp(Data.CCDpath2(end),'\')%because stupid error
        Data.CCDpath2=[Data.CCDpath2 '\'];
    end
    %Store the filename containing the bleaching image from the matlab file in userdata
    Data.CCDname2=S.CCDname;
    if size(Data.CCDname2,2)>0 && ~strcmp(Data.CCDname2(end-3),'.') %because stupid error
        Data.CCDname2=[Data.CCDname2 '.tif'];
    end
    
    %If the path does not exist (which can happen when moving stuff, the
    %option is given to load another file
    if ~exist(Data.CCDpath2, 'dir') || size(Data.CCDname2,2)==0 ; 
        [CCDname2, CCDpath2]=uigetfile({'*.TIF;*.tif','Tif image (*.TIF,*.tif)'},'select image file');
        Data.CCDpath2=CCDpath2;
        Data.CCDname2=CCDname2;
    end
    
    %Load the image and store the size of the y and x-axis in the userdata
    img1=imread([Data.CCDpath2 Data.CCDname2],1);
    Data.xaxis=size(img1,2);
    Data.yaxis=size(img1,1);
    
    %If a zoomfield is present, reset the zoom by removing the zoom field
    if isfield(Data,'x_box')
        Data=rmfield(Data,'x_box');
        Data=rmfield(Data,'y_box');
    end

    %Save the userdata and enable the Go button
    set(Counter,'UserData',Data);
    set(pushbutton_Go,'enable','on')
    set(text5,'string',[fp2 fn2])
end
%redraw the image
drawimage_Counter
function pushbutton_LoadBleach_Callback(hObject, eventdata, handles)
%This functions loads the bleaching image independent of the localized
%emitters (useful for intensity matching or stepwise bleaching matching
Counter=findobj('Tag','Counter');
pushbutton_Go_intensity=findobj(Counter,'Tag','pushbutton_Go_intensity');
pushbutton_Go_steps=findobj(Counter,'Tag','pushbutton_Go_steps');
text8=findobj(Counter,'Tag','text8');

%Load the bleaching image
[fn3, fp3]=uigetfile('*.*','select bleaching image');
if fp3>0
    cd(fp3)
    Data=get(Counter,'UserData');
    
    %store the locations in userdata
    Data.fn3=fn3;
    Data.fp3=fp3;
    
    %enable buttons that use this image
    set(pushbutton_Go_intensity,'enable','on')
    set(pushbutton_Go_steps,'enable','on')
    
    %display the path
    set(text8,'string',[fp3 fn3])
    
    %read image and store the imagesize in userdata
    img1=imread([fp3 fn3],1);
    Data.xaxis=size(img1,2);
    Data.yaxis=size(img1,1);
    
    %store userdata
    set(Counter,'UserData',Data)
end
drawimage_Counter


%Process the seperate functions
function pushbutton_Go_Callback(hObject, eventdata, handles)
Overlap_function('run_counter');

function pushbutton_gosteps_Callback(hObject, eventdata, handles)
Overlap_function('count_steps')
function pushbutton_intensity_Callback(hObject, eventdata, handles)
Overlap_function('intensity')
